import java.io.*;
import java.util.regex.*;

/**
 * Класс для поиска идентификаторов
 *
 * @author Коленционок С.К.
 */
public class Identifier {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new FileReader("E:\\work\\Splite\\src\\Maine.java"));
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("output.txt"));
        String string;
        Pattern p = Pattern.compile(".+");
        Matcher matcher = p.matcher("");

        Pattern id = Pattern.compile("[A-Za-z_]+\\w*");
        Matcher idFinder = id.matcher("");

        Pattern patternQuotes = Pattern.compile("\\\".+?\\\"");
        Matcher matcherQuotes = patternQuotes.matcher("");

        while ((string = bufferedReader.readLine()) != null) {
            matcher.reset(string);
            while (matcher.find()) {
                bufferedWriter.write(matcher.group());
            }
        }
        bufferedReader.close();
        bufferedWriter.close();

        bufferedReader = new BufferedReader(new FileReader("output.txt"));
        bufferedWriter = new BufferedWriter(new FileWriter("Identifier.txt"));
        while ((string = bufferedReader.readLine()) != null) {
            idFinder.reset(string);
            matcherQuotes.reset(string);
            while (matcherQuotes.find()) {
                string = string.replaceAll("\".+?\"", "");
            }
            while (idFinder.find()) {
                bufferedWriter.write(idFinder.group() + "\n");
            }
        }


        bufferedReader.close();
        bufferedWriter.close();
    }
}
